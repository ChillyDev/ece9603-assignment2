import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import seaborn as sns
from keras.preprocessing.sequence import TimeseriesGenerator
from scipy.fftpack import fft
from sklearn.preprocessing import StandardScaler

pd.set_option('display.max_columns', 500)
finalDayOfFirstYearIdx = 8756

def loadDataDf(dataName):
    outputName = dataName + "_MW"
    dataPath = "Data/" + dataName + "_hourly.csv"

    return pd.read_csv(dataPath), outputName

def expandDatetime(df):
    timeSeries = pd.to_datetime(df["Datetime"])
    df["Year"] = timeSeries.dt.year
    df["Month"] = timeSeries.dt.month
    #df["DayOfMonth"] = timeseries.dt.day
    df["Day"] = timeSeries.dt.weekday
    df["Hour"] = timeSeries.dt.hour

    return df

def dayIdxToName(idx):
    dayList = ["Monday",
               "Tuesday",
               "Wednesday",
               "Thursday",
               "Friday",
               "Saturday",
               "Sunday"]
    return dayList[idx]

def monthIdxToName(idx):
    monthList = ["Invalid",
                 "January",
                 "February",
                 "March",
                 "April",
                 "May",
                 "June",
                 "July",
                 "August",
                 "September",
                 "October",
                 "November",
                 "December"]
    return monthList[idx]

def mapDayAndMonth(df):
    df["Month"] = df["Month"].apply(lambda x: monthIdxToName(x))
    df["Day"] = df["Day"].apply(lambda x: dayIdxToName(x))

    return df

def degreesToRadians(degrees):
    return degrees * (2*math.pi)/360

def hourOfDayTo2D(hour):
    # 360 degrees / 24 hours = 15 degrees per hour
    timeInDegrees=hour*15

    timeX=math.cos(degreesToRadians(timeInDegrees))
    timeY=math.sin(degreesToRadians(timeInDegrees))

    return timeX, timeY

def dayOfMonthTo2D(day, lastDayOfMonth):
    dayInDegrees = day * (360 / lastDayOfMonth)

    dayOfWeekX=math.cos(degreesToRadians(dayInDegrees))
    dayOfWeekY=math.sin(degreesToRadians(dayInDegrees))

def oneHotEncodeDayAndMonth(df):
    df = df.join(pd.get_dummies(df.Month))
    df = df.join(pd.get_dummies(df.Day))
    df = df.drop(['Datetime', 'Month', 'Day'], axis='columns')

    return df

def oneHotEncode2dTime(df):
    df['2D Time'] = df["Hour"].apply(lambda x: hourOfDayTo2D(x))
    df['Time X'] = df['2D Time'].apply(lambda x: x[0])
    df['Time Y'] = df['2D Time'].apply(lambda x: x[1])
    df = df.drop(['2D Time', 'Hour'], axis='columns')

    return df

def oneHotEncode2dDayOfMonth(df):
    df['2D_DayOfMonth'] = df["DayOfMonth"].apply(lambda x: dayOfMonthTo2D(x))
    df['DayOfMonth_X'] = df['2D_DayOfMonth'].apply(lambda x: x[0])
    df['DayOfMonth_Y'] = df['2D_DayOfMonth'].apply(lambda x: x[1])
    df = df.drop(['2D_DayOfMonth', 'DayOfMonth'], axis='columns')

    return df

def preprocessData(df):
    df = df.sort_values(by=['Datetime']).reset_index(drop=True)
    df = expandDatetime(df)
    df = mapDayAndMonth(df)
    df = oneHotEncodeDayAndMonth(df)
    df = oneHotEncode2dTime(df)

    return df

def splitByYears(df, dataName, firstYear, lastYear, dropLabel=True):
    startIdx = df.loc[df['Year']==firstYear].head(1).index[0]
    stopIdx = df.loc[df['Year']==lastYear].head(1).index[0]

    if dropLabel:
        return (df.drop([dataName], axis='columns').loc[startIdx:stopIdx-1],
                df.drop([dataName], axis='columns').loc[stopIdx:df.tail(1).index[0]],
                df[dataName].loc[startIdx:stopIdx-1],
                df[dataName].loc[stopIdx:df.tail(1).index[0]])

    return (df.loc[startIdx:stopIdx-1],
            df.loc[stopIdx-24:df.tail(1).index[0]],
            df[dataName].loc[startIdx:stopIdx-1],
            df[dataName].loc[stopIdx:df.tail(1).index[0]])

# Scale the input data
def scaleFeatures(train, test):
    scaler = StandardScaler()
    train_scaled = scaler.fit_transform(train)
    test_scaled = scaler.transform(test)

    return train_scaled, test_scaled

from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error

#accuracy = accuracy_score(y_test, y_pred)
def getPerformanceMetrics(y_test, y_pred):
    r2 = r2_score(y_test, y_pred)
    mae = mean_absolute_error(y_test, y_pred)
    rmse = math.sqrt(mean_squared_error(y_test, y_pred))

    return (r2, mae, rmse)

def printPerformanceMetrics(r2, mae, rmse):
    print("R2 Score: {} \nMean Absolute Error: {} \nRoot Mean Squared Error: {}\n".format(r2,
                                                                                          mae,
                                                                                          rmse))

def plotResultsVsActual(df, y_pred, testYear=2018,
                        dataPath='Data/PJME_hourly.csv', dataName='PJME_MW'):
    og_df = pd.read_csv(dataPath).sort_values(by=['Datetime']).reset_index(drop=True)

    plotdf = pd.DataFrame(
        {'Set': 'Actual',
         'Time': og_df.loc[df['Year']==testYear]['Datetime'],
         'Power (MW)': og_df.loc[df['Year']==testYear][dataName]}).append(pd.DataFrame(
        {'Set': 'Predicted',
         'Time': og_df.loc[df['Year']==testYear]['Datetime'],
         'Power (MW)': y_pred})).reset_index(drop=True)

    fig = plt.figure(figsize=(30, 20))
    ax = fig.add_subplot(1, 1, 1)

    snsplot = sns.lineplot(data=plotdf, x='Time', y='Power (MW)', hue='Set', ax=ax)

def evaluateModelPerformance(model, df, x_test, y_test, dataName='PJME_MW'):
    # Evaluate Performance
    y_pred = model.predict(x_test)
    r2, mae, rmse = getPerformanceMetrics(y_test, y_pred)
    printPerformanceMetrics(r2, mae, rmse)
    plotResultsVsActual(df, y_pred.reshape(len(y_test),), dataName=dataName)

## Assignment 2 Specific Content for RNN's
def logTimeseriesBatch(gen, batches=5):
    print("Number of batches: {}\nChecking first {}".format(len(gen), batches))

    for i in range(batches):
        x,y = gen[i]
        print('{} => {}'.format(x,y))

def splitByIndex(df, dataName, startTest, stopTest):
    return (df.loc[0:startTest-1],
            df.loc[startTest-25:stopTest-1],
            df[dataName].loc[0:startTest-1],
            df[dataName].loc[startTest:stopTest-1])

def startStopForFirstMonthTraining():
    hoursInWeek = 24*7
    finalDayOfFirstMonthIdx = 742
    startTestRegion = finalDayOfFirstMonthIdx - 1 # For training data in the RNN we'll just use the first
                                        # month of 2002 (subtract one since dataset
                                        # starts at 1am rather than 12am)
    stopTestRegion = startTestRegion + hoursInWeek  - 1 # The test set will be the first week of the February of
                                                        # 2002 following the January of 2002 used in the training
                                                        # set
    return (startTestRegion, stopTestRegion)

def getTimeSeriesTrainTest(df, dataName):
    startTestRegion, stopTestRegion = startStopForFirstMonthTraining()

    x_train, x_test, y_train, y_test = splitByIndex(df, dataName,
                                                    startTestRegion,
                                                    stopTestRegion)
    print(x_train)
    x_train_scaled, x_test_scaled = scaleFeatures(x_train, x_test)

    return (x_train_scaled,
            x_test_scaled,
            y_train,
            y_test)

def plotResultsVsActualRNN(df, y_pred, dataPath='Data/PJME_hourly.csv',
                           dataName='PJME_MW'):
    og_df = pd.read_csv(dataPath).sort_values(by=['Datetime']).reset_index(drop=True)

    startTestRegion, stopTestRegion = startStopForFirstMonthTraining()
    plotdf = pd.DataFrame(
        {'Set': 'Actual',
         'Time': og_df['Datetime'][startTestRegion:stopTestRegion],
         'Power (MW)': og_df[dataName][startTestRegion:stopTestRegion]}).append(pd.DataFrame(
        {'Set': 'Predicted',
         'Time': og_df['Datetime'][startTestRegion:stopTestRegion],
         'Power (MW)': y_pred.flatten()})).reset_index(drop=True)

    fig = plt.figure(figsize=(30, 20))
    ax = fig.add_subplot(1, 1, 1)

    snsplot = sns.lineplot(data=plotdf, x='Time', y='Power (MW)', hue='Set', ax=ax)
    return fig
