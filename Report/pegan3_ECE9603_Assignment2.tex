\documentclass[
	12pt, % Default font size, values between 10pt-12pt are allowed
	%letterpaper, % Uncomment for US letter paper size
]{article}

\usepackage{graphicx}
\usepackage{cite}
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{textcomp}

\graphicspath{{figures}}
\hypersetup{hidelinks}
\bibliographystyle{IEEEtran}

% Assignment Information
\title{ECE9603 Assignment 2 - Neural Networks}
\author{Patrick Egan - 250 797 509}
\date{November 14, 2021}

\begin{document}
\maketitle % Generate assignment header

\section*{Forecasting Problem}
\indent With more and more electronic devices becoming common place in households it's
important to make sure a power grid is capable of providing enough power to
every home.  If too much of a power consumption demand is made a grid can go down
resulting in large power outages.  By using the hourly power consumption data it
becomes possible to identify trends and seasonality in the data which may be
used to forecast future hourly power consumption trends.  If power consumption
demands can be predicted then it can be possible to prepare for such large
demands at peak hours.

\section*{Data Selection and Preparation}
\indent The data used in the selected Hourly Enery Consumption dataset~\cite{power_consumption_dataset},
is the hourly power consumed in Megawatts (MW) (dataset name has a typo, should
be power rather than energy as energy is measured in Joules not Watts) for the PJM East
region of the United States covering Pensylvania, New Jersey and Maryland.
The data is comprised of a timestamp including the date and hour for the sample
along with the power consumed for that hour, as shown in Fig.~\ref{raw_dataset}.
This dataset contains 145366 rows or hours of power consumption samples which
covers the beginning of January in 2002 through to the beginning of August in
2018.
\\ \indent To apply this data to regression focused models the timestamp was
expanded into usable features.  The year was left as a regular integer while the
month and day of the week were encoded into their own one hot encoder sets of
features.  The time was initially tested as simply an integer as well, but was
later transformed into complex numbers representing positions on a circle to
preserve the relative closeness of the 23rd and 0th hours on a 24 hour clock as
described in~\cite{time_feature_engineering}.  The real and imaginary or x and
y components of these circle time representation values were saved as two
separate features, time X and time Y.  Lastly, standardization was applied to
the features using the training set after the power value was dropped.
\\ \indent In order to use these samples with a recurrent neural network a few
additional steps needed to be taken.  Firstly, the power consumption feature
which was used only as a label for the models in the previous assignment was added
back to the samples to be used in predicting future values.  Additionally, the
samples needed to be grouped together into batches in order to be passed into
the neural network.  In this assignment, the time series data was arranged into
batches of twenty four samples sliding one sample forward for each batch.  The
power consumption of the sample following the batch of twenty four samples in the
time series was used as the target value for each batch.  The set used for
testing the recurrent neural network models was also arranged into batches using
time series data starting from the end of the period used for the training data.
\\ \indent It should be noted that when training and testing the recurrent neural
network models, only the first month of the data set (january 2002) was used for
the training data and the first week of the following month (february 2002) was
used for the test data.  This is because of how long it takes to train a
recurrent neural network.

\begin{figure}[tp]
  \centerline{\includegraphics[scale=0.4]{raw_data.png}}
  \caption{The PJME regional hourly power consumption data unprocessed}
  \label{raw_dataset}
\end{figure}

\section*{Selected Network Architecture}
\indent Although a feed forward neural network was selected for comparison against
other models in the previous assignment it seemed like a recurrent neural network
may yield better results for a time series forecasting problem like this one.
Like a feed forward neural network, a recurrent network uses multiple layers of
interconnected 'neurons' with activation functions to make decisions.  Where a
recurrent differs is in its structure.  In a more traditional feed forward
neural network, data propagates from one
layer to the next, one sample at a time, and eventually to the output in a
stateless fashion.  A recurrent neural network, however, uses a structure which
maintains a state in each layer and generally expects input data to exist in a
state prior to the state of the output data (with the exception of some specific
applications like bidirectional recurrent neural networks).  This makes recurrent
neural networks ideal for time series data as it may utilize previous samples in
the time series to create batches which provide context for predicting future
values.
\\ \indent The selected architecture for the recurrent neural network in this
assignment used multiple time series values as inputs to predict the next
subsequent power consumption value.  Multiple inputs seemed to be a wise decision
as the dataset is composed of consistent hourly samples where a sliding window could
be applied to create batches that would provide a contextual time period for
predicting subsequent values from any point in the dataset.  Single time step
inputs could still be used with the recurrent neural network model but would not
have the same advantage of previous time steps to provide additional context.
Singular outputs were selected over multiple outputs for the architecture to
better utilize the sliding window technique because it would allow for smaller
steps.  Smaller steps would allow the model to reuse steps from previous contexts
while predicting multiple values would require moving the sliding window further
and preserve less of the current context in the next step.
\\ \indent To reduce training time for each model in this assignment, a relatively
small recurrent neural network will be used.  These networks will expect a batch
of 24 adjacent samples as input with one to three gated recurrent unit (or GRU)
layers and a single dense feed forward layer as the output layer to predict the
future power consumption value.
GRU layers were selected over long short term memory (LSTM) layers for having
less internal values, making them faster to train.  GRU layers were also selected
over the more traditional Elmann recurrent unit due to the vanishing and exploding
gradient issues associated with it.  After each GRU layer a dropout layer is used
for regularizing the outputs before feeding them into the next layer.
\\ \indent The hyperparameters of interest which were tuned in this assignment
included the shape of the architecture in terms of hidden layers and neurons,
activation functions for the hidden layers and the optimizer algorithm
used in the model training process.  With regard to network architecture adjustments,
having more hidden layers  and neurons can help a neural network 'save' more patterns to look
for that occur in the data.  However, too many hidden layers and neurons can lead to
overfitting because the neural network may 'save' enough patterns from the
training data that it becomes less generalized for new patterns in unseen data.
An activation function is the function run
by each neuron in a neural network to make a decision or determine a value based
on the provided inputs and weights connected to the neuron.  An activation
function generally must fit the intention of a neural network in order to be
effective.  For example the softmax function uses inputs to provide probabilities
for all possible outputs which would certainly apply for clasification problems
but not so much for regression problems.  The three activation functions of
interest were the recitified linear unit (relu), exponential linear unit (elu)
and scaled exponential linear unit (selu) functions.
The optimizer algorithm executes the important task of adjusting the weights of
a model as it trains based on tunable values.  The optimizer algorithms of interest
included the popular adaptive momentum estimation (Adam) algorithm and the root
mean square prop algorithm.

\section*{Algorithm Application and Evaluation}
\indent Each model was evaluated using the R\textsuperscript{2} score, Mean
Absolute Error and Root Mean Square Error as provided through scikit-learn~\cite{sklearn}.
The R\textsuperscript{2} score, as shown in (\ref{r2_eqn}) where RSS is equal to
residual sum of squares and TSS is equal to the total sum of squares, was picked as it
provides a good metric to represent the correlation of the forecasted power
consumption to the actual values.  Both the mean absolute error, as shown in
(\ref{mean_abs_err_eqn}), and
root mean square error, as shown in (\ref{root_mean_sqr_err_eqn}), values were
picked to show relative accuracy.  Mean absolute error can be interpreted
as the 'quantity' of error as it involves taking the sum of the absolute value of
each individual error which effectively combines them without canceling each
other out in any way.  Root mean square error, however, represents the 'quality'
of present errors moreso as it enlarges bigger errors in the ``square'' portion.

\begin{equation}
  \label{r2_eqn}
  \centering
  R^{2} = 1 - \frac{RSS}{TSS}
\end{equation}

\begin{equation}
  \label{mean_abs_err_eqn}
  \centering
  MAE = \frac{\sum_{i=1}^{n}|{y_{i}-\hat{y}_{i}}|}{n}
\end{equation}

\begin{equation}
  \label{root_mean_sqr_err_eqn}
  \centering
  RMSE = \sqrt{\frac{\sum_{i=1}^{n}(y_{i}-\hat{y}_{i})^{2}}{n}}
\end{equation}

Alone they provided some helpful information.  When compared against each other
amongst the models they made for good comparison metrics showing 'quantity' vs
'quality'.
\\ \indent The resulting metrics used in evaluation were generated by applying
hold out set validation.  This involved training the models on samples from
January, 2002 the first month of the dataset and then trying to forecast power
consumption values from the first week of February, 2002.  The test set used for
forecasting the power consumption also had the last 24 hours of the training set
appended to the beginning so that the first 24 hours of February, 2002 could be
predicted using the same windowing technique applied to the training set.
\\ \indent In order to compare different possible hyper-parameter configurations
of recurrent neural networks, the grid search algorithm was utilized with wandb~\cite{wandb}.
Grid search is an algorithm which applies every possible combination of a
provided set of hyperparameters to a model and records the performance to
automate the process of finding the best hyperparameters for a model.  Wandb~\cite{wandb}
is a machine learning tool which records individual hyperparameter ``test runs''
and presents them in a tidy web dashboard.

\section*{Results}
Early results showed that regularization would be necessary as a few model
hyperparameter 'test runs' ended up diverging during training as shown in Fig.~\ref{divergent_training_error}.
To address this issue dropout layers were added to the architecture with one
dropout layer after each hidden layer all using a uniform dropout rate of 0.2 which
led to smoother training error curves like the one shown in Fig.~\ref{rnn_best_train_curve}.
\\ \indent While the shape of the architecture was tuned, the
number of layers and number of neurons per layer were adjusted.  Three recurrent hidden layers
appeared to perform the best compared to the performances of models using one
or two hidden layers instead as shown in Table~\ref{tuning_layers_table}.
This makes sense as having more hidden layers allows the model to form more
internal relationships to model particular patterns in the data.  In regard to
different combinations of neurons in each hidden layer it appeared that overall
models with the neuron and layer arrangement 24 \textrightarrow 12 \textrightarrow 12
performed better than their counterparts with some sample hyperparameter test
runs shown in Table~\ref{tuning_neurons_table}.  This could be due to overfitting
for architectures using more neurons and underfitting for those using less
neurons.
\\ \indent Comparing the usage of different activation function parameters showed
that some model performance gains could be made.  Interestingly, changing the
output layer's activation function yielded better improvements while adjusting
the hidden layers' activation functions didn't yield results as significant.
From some of the highest performing hyperparameter configurations it appeared
that the exponential linear unit (elu) yielded the best performances when applied
to the output layer compared to the rectified linear unit (relu) and scaled exponential
linear unit (selu) activation functions as shown in Table~\ref{tuning_activation_table}.
The optimizer algorithms also proved to not have many major advantages over each
other with minimal tuning as they were used with default values.  The best
performing hyperparameter configuration is shown in Table~\ref{tuning_best_table}
with its predictions for the test set shown in Fig~\ref{rnn_best_rmsprop} and
training error curve shown in Fig~\ref{rnn_best_train_curve}.
\\ \indent It is difficult to compare the results of the rnn models trained in
this assignment to the models used in the previous assignment because of the
difference in training and testing data.  Overall the rnn models seemed to
identify and predict the smaller periodic patterns better than the models in
assignment 1 from observing the prediction plots.  Otherwise the gathered
performance metrics in this assignment unfortunately cannot be used as a fair
comparison against the performance metrics from assignment 1.
\\ \indent In hindsight the tuning process may have seen better results if more
window sizes were tested as input batches.  The 24 step window was selected due
to the periodic patterns that occurred on a daily basis.  Using a timeseries
batch with a day's worth of context may not properly represent the periodic
patterns that occur in a month or year, however.  Additionally, using a larger
training set could have certainly improved performance as only one month was
used to train each model in this assignment.  This was of course due to the large
amount of time required to train each individual model as a result of the
computational demand of backpropagation through time for recurrent neural networks.

\bibliography{assignment_ref}

\newpage
\section*{Appendices}

\begin{table}[ht]
  \caption{Best Hyperparameter Configuration}
  \label{tuning_best_table}
  \centering
  \resizebox{\textwidth}{!}{\begin{tabular}{|l|c|}
    \hline \hline
    \textbf{Hyperparameter} & \textbf{Value} \\
    \hline \hline
    Number of Hidden Layers & 3 \\
    \hline
    Neuron Arrangement      & 24 \textrightarrow 12 \textrightarrow 12 \\
    \hline
    Output Layer Activation Function & exponential linear unit (elu) \\
    \hline
    Optimizer Algorithm     & rmsprop \\
    \hline
    \hline

    \textbf{Metric} & \textbf{Value} \\
    \hline
    R\textsuperscript{2}   & 0.6516   \\
    \hline
    Mean Absolute Error    & 1839.781 \\
    \hline
    Root Mean Square Error & 2350.675 \\
    \hline \hline
  \end{tabular}}
\end{table}

%% Performance comparison tables
\begin{table}
  \caption{Model Layer Tuning Result Metrics}
  \label{tuning_layers_table}
  \centering
  \resizebox{\textwidth}{!}{\begin{tabular}{|l|c|c|c|}
    \hline \hline
    \textbf{Metric} & \textbf{1 hidden layer}
    & \textbf{2 hidden layers}
    & \textbf{3 hidden layers} \\
    \hline \hline
    R\textsuperscript{2}   & -0.2026  & 0.1045   & 0.6165 \\
    \hline
    Mean Absolute Error    & 3661.441 & 3288.233 & 1981.597 \\
    \hline
    Root Mean Square Error & 4367.319 & 3768.582 & 2466.184 \\
    \hline \hline
  \end{tabular}}
  {\raggedright \footnotesize \centering The models tested with these different
    amounts of layers all used GRU layers with relu as the activation function
    for both the
    hidden and output layers, 12 neurons in each layer and the rmsprop optimizer
    algorithm\par}
\end{table}

\begin{table}
  \caption{Model Neuron Tuning Result Metrics}
  \label{tuning_neurons_table}
  \centering
  \resizebox{\textwidth}{!}{\begin{tabular}{|l|c|c|c|c|}
    \hline \hline
    \textbf{Metric} & \textbf{24 \textrightarrow 24 \textrightarrow 24}
    & \textbf{24 \textrightarrow 24 \textrightarrow 12}
    & \textbf{24 \textrightarrow 12 \textrightarrow 12}
    & \textbf{24 \textrightarrow 12 \textrightarrow 6} \\
    \hline \hline
    R\textsuperscript{2}   & -2.205   & -1.509   & 0.5836   & -0.8726 \\
    \hline
    Mean Absolute Error    & 5960.101 & 5960.101 & 2100.52  & 5000.376 \\
    \hline
    Root Mean Square Error & 6308.613 & 6308.613 & 2569.783 & 5449.618 \\
    \hline \hline
  \end{tabular}}
  {\raggedright \footnotesize \centering The models tested in this table are
    presented with their architectures described in terms of neurons at each
    layer where the leftmost number represents the amount of neurons in the
    first hidden layer and the rightmost number represents the number of neurons
    in the final hidden layer before the output layer.  Each of these models
    were trained using GRU layers with the relu activation function in both the
    hidden and output layers along with the adam optimization algorithm\par}
\end{table}

\begin{table}
  \caption{Model Output Layer Activation Function Tuning Results Metrics}
  \label{tuning_activation_table}
  \centering
  \resizebox{\textwidth}{!}{\begin{tabular}{|l|c|c|c|}
    \hline \hline
    \textbf{Metric} & \textbf{rectified linear unit}
    & \textbf{exponential linear unit}
    & \textbf{scaled linear unit} \\
    \hline \hline
    R\textsuperscript{2} & 0.5836 & 0.5944 & 0.6463 \\
    \hline
    Mean Absolute Error & 2100.052 & 1976.818 & 1891.653 \\
    \hline
    Root Mean Square Error & 2569.783 & 2536.308 & 2368.369 \\
    \hline \hline
  \end{tabular}}
  {\raggedright \footnotesize \centering The models tested with these activation
    functions in the output layer and used a 24 \textrightarrow 12 \textrightarrow 12
    architecture with the adam optimization algorithm and the relu activation function
    in the hidden layers\par}
\end{table}

\begin{figure}
  \centerline{\includegraphics[scale=0.18]{no_regularization_divergent_training_error.png}}
  \caption{A hyperparameter test run where the model had no regularization which put
    it at risk for experiencing a divergence during training as shown}
  \label{divergent_training_error}
\end{figure}

\begin{figure}
  \centerline{\includegraphics[scale=0.18]{best_rnn_train_curve.png}}
  \caption{Training error curve of the best performing RNN.  This model
    used the \textbf{GRU} layer for the recurrent
    layers with the \textbf{elu} activation function for the three hidden layers and
    the \textbf{elu} activation function for the output layer
    along with the \textbf{rmsprop} optimizer algorithm}
  \label{rnn_best_train_curve}
\end{figure}

%% Prediction Plot Figures
\begin{figure}
  \centerline{\includegraphics[scale=0.15]{rnn_1_layer.png}}
  \caption{Single Layer RNN Performance using a \textbf{GRU} layer with the
    \textbf{relu} activation function for both the hidden and output layers along with
    the \textbf{rmsprop} optimizer algorithm.  Predicted hourly power consumption
    (orange) is plotted against the actual power consumption for that week (blue)}
  \label{rnn_1_layer}
\end{figure}

\begin{figure}
  \centerline{\includegraphics[scale=0.15]{rnn_2_layer.png}}
  \caption{Two Layer RNN Performance using \textbf{GRU} layers for the recurrent
    layers with the
    \textbf{relu} activation function for both the hidden and output layers along with
    the \textbf{rmsprop} optimizer algorithm.  Predicted hourly power consumption
    (orange) is plotted against the actual power consumption for that week (blue)}
  \label{rnn_2_layer}
\end{figure}

\begin{figure}
  \centerline{\includegraphics[scale=0.15]{rnn_best_prediction_w_rmsprop.png}}
  \caption{Hourly power consumption prediction for best performing RNN.
    This model used the \textbf{GRU} layer for
    the recurrent layers with the \textbf{elu} activation function for the three
    hidden layers and the \textbf{elu} activation function for the output layer
    along with the \textbf{rmsprop} optimization algorithm.  Predicted hourly power
    consumption (orange) is plotted against the actual power consumption for
    that week (blue)}
  \label{rnn_best_rmsprop}
\end{figure}
\newpage

%% Assignment 1 Table and Figures
\begin{table}[ht!]
  \caption{Assignment 1 Model Results Metrics using samples from the years 2002
    through 2017 with the samples from 2018 as the test set to predict}
  \label{metrics_table}
  \centering
  \resizebox{\textwidth}{!}{\begin{tabular}{|l|c|c|c|}
    \hline \hline
    \textbf{Metric} & \textbf{Linear Regression}
    & \textbf{Random Forest Regression}
    & \textbf{Artificial Neural Network} \\
    \hline \hline
    R\textsuperscript{2} & 0.49408 & 0.59135 & 0.64048 \\
    \hline
    Mean Absolute Error & 3536.636 & 2978.883 & 2913.126 \\
    \hline
    Root Mean Square Error & 4466.279 & 4014.043 & 3764.983 \\
    \hline \hline
  \end{tabular}}
  {\raggedright \footnotesize \centering This data was gathered in assignment-1 using the final year
    as the test set\par}
\end{table}

\begin{figure}[hb]
  \centerline{\includegraphics[scale=0.18]{linear_regression_plot.png}}
  \caption{The power consumption for the 2018 as predicted by the Linear
    Regression model (orange) plotted against the actual power consumption for
    that year (blue)}
  \label{linear_regression_plot}
\end{figure}

\begin{figure}
  \centerline{\includegraphics[scale=0.18]{random_forest_plot.png}}
  \caption{The power consumption for the 2018 as predicted by the Random
    Forest Regression model (orange) plotted against the actual power consumption for
    that year (blue)}
  \label{random_forest_plot}
\end{figure}

\begin{figure}
  \centerline{\includegraphics[scale=0.18]{ann_plot.png}}
  \caption{The power consumption for the year of 2018 as predicted by the
    Artificial Neural Network model (orange) plotted against the actual power
    consumption for that year (blue)}
  \label{neural_network_plot}
\end{figure}

\end{document}
